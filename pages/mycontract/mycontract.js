// pages/mycontract/mycontract.js.js
Page({
  changeFlag () {
    this.setData({
      flag: true
    })
  },
  closeDialog () {
    this.setData({
      flag: false
    })
  },
  lookContract () {
    wx.navigateTo({
      url: '../compact/compact'
    })
  },
  changeBind (event) {
    let tit = event.target.dataset.tit;
    if (tit == '查看发票') {
      wx.navigateTo({
        url: '../checkinvoice/checkinvoice'
      })
    } else {
      wx.navigateTo({
        url: '../applycontract/applycontract'
      })
    }
    // wx.navigateTo({
    //   url: '../applycontract/applycontract'
    // })
  },
  /**
   * 页面的初始数据
   */
  data: {
    flag: false,
    arrData: [
      {
        id: 1,
        serial: 21321,
        address: '北京11',
        money: '1232.00',
        date: '2017-12-01',
        flag: true,
        tit: '查看发票'
      },
      {
        id: 2,
        serial: 2132321,
        address: '北京22',
        money: '1232.00',
        date: '2017-12-02',
        flag: true,
        tit: '申请发票'
      },
      {
        id: 3,
        serial: 2135621,
        address: '北京33',
        money: '12.00',
        date: '2017-12-4',
        flag: true,
        tit: '查看发票'
      },
      {
        id: 4,
        serial: 213821,
        address: '北京44',
        money: '4533.00',
        date: '2017-12-5',
        flag: false,
        tit: '申请发票'
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})